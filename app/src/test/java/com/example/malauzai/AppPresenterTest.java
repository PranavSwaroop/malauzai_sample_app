package com.example.malauzai;

import com.example.malauzai.presenter.AppContract;
import com.example.malauzai.presenter.AppPresenter;
import com.example.malauzai.model.MarsPhotos;
import com.example.malauzai.retrofit.NetworkUtil;
import com.example.malauzai.retrofit.RetrofitService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppPresenterTest {

    @Mock
    RetrofitService retrofitService;

    @Mock
    AppContract.MainActivityViewContract mainActivityViewContract;

    @Mock
    NetworkUtil networkUtil;

    @InjectMocks
    AppPresenter appPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void noInternetConnection() {
        when(networkUtil.isConnectionAvailable()).thenReturn(false);
        appPresenter.loadItems("1");
        verify(mainActivityViewContract).connectionIsNotAvailable();
    }

    @Test
    public void withInternetConnection() {
        when(networkUtil.isConnectionAvailable()).thenReturn(true);
        appPresenter.loadItems("1");
        verify(mainActivityViewContract).showProgress();
        verify(retrofitService).loadItems("1");
    }

    @Test
    public void onSuccess() {
        MarsPhotos marsPhotos = mock(MarsPhotos.class);
        appPresenter.onSuccess(marsPhotos);
        verify(mainActivityViewContract).displayData(marsPhotos);
        verify(mainActivityViewContract).hideProgress();
    }

    @Test
    public void onFailed() {
        appPresenter.onFailed();
        verify(mainActivityViewContract).onDataLoadFailed();
    }
}