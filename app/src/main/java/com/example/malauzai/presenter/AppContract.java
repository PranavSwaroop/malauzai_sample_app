package com.example.malauzai.presenter;

import com.example.malauzai.model.MarsPhotos;

public interface AppContract {
    public interface MainActivityViewContract {
        void showProgress();
        void hideProgress();
        void connectionIsNotAvailable();
        void displayData(MarsPhotos marsPhotos);
        void onDataLoadFailed();
    }

    public interface MainActivityPresenterContract {
        void loadItems(String pageIndex);
        void loadSpirits(String pageIndex);
        void loadOpportunityItems(String pageIndex);
    }
}
