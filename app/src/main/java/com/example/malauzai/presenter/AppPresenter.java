package com.example.malauzai.presenter;

import androidx.annotation.VisibleForTesting;

import com.example.malauzai.retrofit.NetworkUtil;
import com.example.malauzai.retrofit.RetrofitService;
import com.example.malauzai.model.MarsPhotos;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppPresenter implements AppContract.MainActivityPresenterContract {

    private AppContract.MainActivityViewContract mainActivityViewContract;
    private NetworkUtil networkUtil;
    private RetrofitService retrofitService;

    public AppPresenter(AppContract.MainActivityViewContract mainActivityViewContract,
                        NetworkUtil networkUtil, RetrofitService retrofitService) {
        this.mainActivityViewContract = mainActivityViewContract;
        this.networkUtil = networkUtil;
        this.retrofitService = retrofitService;
    }

    @Override
    public void loadItems(String pageIndex) {
        if(networkUtil.isConnectionAvailable()) {
            mainActivityViewContract.showProgress();
            Call<MarsPhotos> marsPhotosCall = retrofitService.loadItems(pageIndex);
            if(marsPhotosCall != null) {
                marsPhotosCall.enqueue(new Callback<MarsPhotos>() {
                    @Override
                    public void onResponse(Call<MarsPhotos> call, Response<MarsPhotos> response) {
                        onSuccess(response.body());
                    }

                    @Override
                    public void onFailure(Call<MarsPhotos> call, Throwable t) {
                        onFailed();
                    }
                });
            }
        } else {
            mainActivityViewContract.connectionIsNotAvailable();
        }
    }

    @Override
    public void loadSpirits(String pageIndex) {
        if(networkUtil.isConnectionAvailable()) {
            mainActivityViewContract.showProgress();
            Call<MarsPhotos> marsPhotosCall = retrofitService.loadSpirits(pageIndex);
            if(marsPhotosCall != null) {
                marsPhotosCall.enqueue(new Callback<MarsPhotos>() {
                    @Override
                    public void onResponse(Call<MarsPhotos> call, Response<MarsPhotos> response) {
                        onSuccess(response.body());
                    }

                    @Override
                    public void onFailure(Call<MarsPhotos> call, Throwable t) {
                        onFailed();
                    }
                });
            }
        } else {
            mainActivityViewContract.connectionIsNotAvailable();
        }
    }

    @Override
    public void loadOpportunityItems(String pageIndex) {
        if(networkUtil.isConnectionAvailable()) {
            mainActivityViewContract.showProgress();
            Call<MarsPhotos> marsPhotosCall = retrofitService.loadOpportunityItems(pageIndex);
            if(marsPhotosCall != null) {
                marsPhotosCall.enqueue(new Callback<MarsPhotos>() {
                    @Override
                    public void onResponse(Call<MarsPhotos> call, Response<MarsPhotos> response) {
                        onSuccess(response.body());
                    }

                    @Override
                    public void onFailure(Call<MarsPhotos> call, Throwable t) {
                        onFailed();
                    }
                });
            }
        } else {
            mainActivityViewContract.connectionIsNotAvailable();
        }
    }

    @VisibleForTesting
    public void onSuccess(MarsPhotos response) {
        mainActivityViewContract.displayData(response);
        mainActivityViewContract.hideProgress();
    }

    public void onFailed() {
        mainActivityViewContract.onDataLoadFailed();
    }
}
