package com.example.malauzai.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.malauzai.R;
import com.example.malauzai.adapter.PhotoAdapter;
import com.example.malauzai.model.MarsPhotos;
import com.example.malauzai.model.Photo;
import com.example.malauzai.presenter.AppContract;
import com.example.malauzai.presenter.AppPresenter;
import com.example.malauzai.retrofit.NetworkUtil;
import com.example.malauzai.retrofit.RetrofitUtil;
import com.example.malauzai.viewmodel.PhotoViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements AppContract.MainActivityViewContract {

    private static final String KEY_CURIOSITY_INDEX = "curiosityIndex";
    private static final String KEY_OPPORTUNITY_INDEX = "opportunityIndex";
    private static final String KEY_SPIRIT_INDEX = "spiritIndex";
    private ProgressDialog progressDialog;
    private AppContract.MainActivityPresenterContract mainActivityPresenter;
    private PhotoAdapter photoAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Parcelable mListState = null;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private PhotoViewModel viewModel;
    private EditText searchInput;
    private int curiosityIndex = 1;
    private int spiritIndex = 1;
    private int opportunityIndex = 1;
    private boolean curiosityPage = true;
    private boolean opportunityPage;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null) {
            curiosityIndex = savedInstanceState.getInt(KEY_CURIOSITY_INDEX);
            opportunityIndex = savedInstanceState.getInt(KEY_OPPORTUNITY_INDEX);
            spiritIndex = savedInstanceState.getInt(KEY_SPIRIT_INDEX);
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        viewModel = ViewModelProviders.of(this).get(PhotoViewModel.class);
        swipeRefreshLayout = findViewById(R.id.swipe_to_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if(curiosityPage) {
                    curiosityIndex = 1;
                    mainActivityPresenter.loadItems(String.valueOf(curiosityIndex));
                    setTitle(getText(R.string.curiosity_name)+"{"+ curiosityIndex +"}");
                } else if(opportunityPage) {
                    opportunityIndex = 1;
                    mainActivityPresenter.loadOpportunityItems(String.valueOf(opportunityIndex));
                    setTitle(getText(R.string.opportunity_name)+"{"+ opportunityIndex +"}");
                } else {
                    spiritIndex = 1;
                    mainActivityPresenter.loadSpirits(String.valueOf(spiritIndex));
                    setTitle(getText(R.string.spirit_name)+"{"+ spiritIndex +"}");
                }
            }
        });
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        photoAdapter = new PhotoAdapter(viewModel.getOldFilteredList());

        recyclerView.setAdapter(photoAdapter);
        searchInput = findViewById(R.id.search_input);
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                viewModel.filterItems(s.toString());
                //mainActivityPresenter.filterItems(s.toString());
            }
        });

        viewModel.getFilteredData().observe(this, new Observer<List<Photo> >() {
            @Override
            public void onChanged(List<Photo> photos) {
                DiffUtil.DiffResult result = DiffUtil.calculateDiff(new PostDiffCallback(viewModel.getOldFilteredList(), photos));
                viewModel.changeOldData();
                result.dispatchUpdatesTo(recyclerView.getAdapter());
                searchInput.requestFocus();
                recyclerView.scrollToPosition(0);
            }
        });


        mainActivityPresenter = new AppPresenter(this,
                new NetworkUtil(this),
                RetrofitUtil.getRetrofitServiceInstance(this));
        //photoAdapter = new PhotoAdapter(mainActivityPresenter.getOldFilteredList());
        loadItems();
        setTitle(getText(R.string.curiosity_name)+"{"+ curiosityIndex +"}");
    }

    class PostDiffCallback extends DiffUtil.Callback {
        private List<Photo> oldList;
        private List<Photo> newList;

        PostDiffCallback(List<Photo> oldList, List<Photo> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return true;
        }
    }

    private void loadItems() {
        mainActivityPresenter.loadItems(String.valueOf(curiosityIndex));
    }

    private void loadOpportunityItems() {
        mainActivityPresenter.loadOpportunityItems(String.valueOf(opportunityIndex));
    }

    private void loadSpiritItems() {
        mainActivityPresenter.loadSpirits(String.valueOf(spiritIndex));
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mListState != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(mListState );
        }
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void connectionIsNotAvailable() {
        Toast.makeText(this, "Connection is not available", Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayData(MarsPhotos marsPhotos) {
        swipeRefreshLayout.setRefreshing(false);
        if(marsPhotos != null) {
            viewModel.setItems(marsPhotos.getPhotos());
            //mainActivityPresenter.setItems(marsPhotos.getPhotos());
        }
    }

    @Override
    public void onDataLoadFailed() {
        Toast.makeText(this, "Data load is failed", Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(savedInstanceState != null)
        {
            mListState = savedInstanceState.getParcelable("ListState");
            recyclerView.getLayoutManager().onRestoreInstanceState(mListState);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.forward_button:

                if(curiosityPage) {
                    curiosityIndex += 1;
                    loadItems();
                    setTitle(getText(R.string.curiosity_name)+"{"+ curiosityIndex +"}");
                } else if (opportunityPage) {
                    opportunityIndex +=1;
                    loadOpportunityItems();
                    setTitle(getText(R.string.opportunity_name)+"{"+ opportunityIndex +"}");
                } else {
                    spiritIndex +=1;
                    loadSpiritItems();
                    setTitle(getText(R.string.spirit_name)+"{"+ spiritIndex +"}");
                }
                recyclerView.scrollToPosition(0);
                break;

            case R.id.back_button:
                if (curiosityPage) {
                    if (curiosityIndex > 1) {
                        curiosityIndex -= 1;
                        loadItems();
                        recyclerView.scrollToPosition(0);
                        setTitle(getText(R.string.curiosity_name)+ "{" + curiosityIndex + "}");
                    } else {
                        Toast.makeText(getApplicationContext(), "Can't go backward further, Page index: 1", Toast.LENGTH_SHORT).show();
                    }
                } else if (opportunityPage) {
                    if (opportunityIndex > 1) {
                        opportunityIndex -= 1;
                        loadItems();
                        recyclerView.scrollToPosition(0);
                        setTitle(getText(R.string.opportunity_name)+ "{" + opportunityIndex + "}");
                    } else {
                        Toast.makeText(getApplicationContext(), "Can't go backward further, Page index: 1", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (spiritIndex > 1) {
                        spiritIndex -= 1;
                        loadItems();
                        recyclerView.scrollToPosition(0);
                        setTitle(getText(R.string.spirit_name)+ "{" + spiritIndex + "}");
                    } else {
                        Toast.makeText(getApplicationContext(), "Can't go backward further, Page index: 1", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.curiosity_rovers:
                curiosityPage = true;
                opportunityPage = false;
                loadItems();
                setTitle(getText(R.string.curiosity_name)+"{"+ curiosityIndex +"}");
                break;

            case R.id.opportunity_rovers:
                loadOpportunityItems();
                curiosityPage = false;
                opportunityPage = true;
                setTitle(getText(R.string.opportunity_name)+"{"+ opportunityIndex +"}");
                break;

            case R.id.spirit_rovers:
                loadSpiritItems();
                curiosityPage = false;
                opportunityPage = false;
                setTitle(getText(R.string.spirit_name)+"{"+ spiritIndex +"}");
                break;


        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CURIOSITY_INDEX, curiosityIndex);
        outState.putInt(KEY_OPPORTUNITY_INDEX, opportunityIndex);
        outState.putInt(KEY_SPIRIT_INDEX, spiritIndex);
    }
}
