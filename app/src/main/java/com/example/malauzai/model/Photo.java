package com.example.malauzai.model;

import com.google.gson.annotations.SerializedName;

public class Photo {
    @SerializedName("id")
    long id;
    @SerializedName("img_src")
    String imageSrc;
    @SerializedName("rover")
    Rover rover;
    @SerializedName("camera")
    Camera camera;

    boolean expanded;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public Rover getRover() {
        return rover;
    }

    public void setRover(Rover rover) {
        this.rover = rover;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
