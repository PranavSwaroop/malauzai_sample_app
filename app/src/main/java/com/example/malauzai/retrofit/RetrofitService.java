package com.example.malauzai.retrofit;

import com.example.malauzai.model.MarsPhotos;
import com.example.malauzai.view.MainActivity;
import com.example.malauzai.view.SampleApplication;
import com.readystatesoftware.chuck.ChuckInterceptor;

import java.util.Map;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RetrofitService {

    String API_KEY = "6OYi9olSpZtIXRHwffuvjv76yNdKVj82k95kGoYa";

    @GET("mars-photos/api/v1/rovers/curiosity/photos?api_key="+API_KEY+"&sol=1000")
    Call<MarsPhotos> loadItems(@Query("page") String pageIndex);

    @GET("mars-photos/api/v1/rovers/spirit/photos?api_key="+API_KEY+"&sol=10")
    Call<MarsPhotos> loadSpirits(@Query("page") String pageIndex);

    @GET("mars-photos/api/v1/rovers/opportunity/photos?api_key="+API_KEY+"&sol=10")
    Call<MarsPhotos> loadOpportunityItems(@Query("page") String pageIndex);


}
