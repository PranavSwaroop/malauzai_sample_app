package com.example.malauzai.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.malauzai.R;
import com.example.malauzai.model.Photo;

import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoViewHolder> {

    private List<Photo> photos;





    public PhotoAdapter(List<Photo> oldFilteredList) {
        photos = oldFilteredList;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_layout, parent, false);
        return new PhotoViewHolder(this, view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        holder.bind(photos.get(position), holder.photo);
    }

    @Override
    public int getItemCount() {
        return photos != null ? photos.size() : 0;
    }


    public List<Photo> getPhotos() {
        return photos;
    }
}
