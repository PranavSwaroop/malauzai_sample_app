package com.example.malauzai.adapter;

import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;

import java.util.ArrayList;
import java.util.List;

class LabelAdapter extends RecyclerView.Adapter<LabelViewHolder> {

    private List<FirebaseVisionImageLabel> labels = new ArrayList<>();

    public LabelAdapter(List<FirebaseVisionImageLabel> labels) {
        this.labels.addAll(labels);
    }


    @NonNull
    @Override
    public LabelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Chip chip = new Chip(parent.getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMarginStart(2);
        params.setMarginEnd(2);
        chip.setLayoutParams(params);


        return new LabelViewHolder(chip);
    }

    @Override
    public void onBindViewHolder(@NonNull LabelViewHolder holder, int position) {
        holder.bind(labels.get(position));
    }

    @Override
    public int getItemCount() {
        return labels.size();
    }


}
