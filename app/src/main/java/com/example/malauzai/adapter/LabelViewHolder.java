package com.example.malauzai.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;

class LabelViewHolder extends RecyclerView.ViewHolder {

    Chip chip;

    public LabelViewHolder(@NonNull View itemView) {
        super(itemView);
        chip = (Chip) itemView;
    }

    void bind(FirebaseVisionImageLabel label) {
        chip.setText(label.getText());
    }
}
