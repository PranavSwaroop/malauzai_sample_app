package com.example.malauzai.adapter;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.malauzai.R;
import com.example.malauzai.model.Photo;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.chip.Chip;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;

import java.util.List;

class PhotoViewHolder extends RecyclerView.ViewHolder {

    ImageView photo;
    Chip photoName;
    Chip id;
    RecyclerView labelContainer;
    ImageView expandCollapseButton;
    View bottomSpace;

    public PhotoViewHolder(final PhotoAdapter photoAdapter, @NonNull View itemView) {
        super(itemView);
        photo = itemView.findViewById(R.id.photo);
        photoName = itemView.findViewById(R.id.photo_name);
        id = itemView.findViewById(R.id.photo_id);
        labelContainer = itemView.findViewById(R.id.labels_container);
        //int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        expandCollapseButton = itemView.findViewById(R.id.expand_collapse_button);
        bottomSpace = itemView.findViewById(R.id.label_bottom_space);
        expandCollapseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (labelContainer.getVisibility() == View.GONE) {
                    photoAdapter.getPhotos().get(getAdapterPosition()).setExpanded(true);
                    labelContainer.setVisibility(View.VISIBLE);
                    bottomSpace.setVisibility(View.VISIBLE);
                    expandCollapseButton.setImageResource(R.drawable.outline_keyboard_arrow_up_black_18dp);
                } else {
                    photoAdapter.getPhotos().get(getAdapterPosition()).setExpanded(false);
                    labelContainer.setVisibility(View.GONE);
                    bottomSpace.setVisibility(View.GONE);
                    expandCollapseButton.setImageResource(R.drawable.outline_keyboard_arrow_down_black_18dp);
                }
            }
        });
    }

    public void bind(final Photo photo, final ImageView photoView) {
        if (photo.isExpanded()) {
            expandCollapseButton.setImageResource(R.drawable.outline_keyboard_arrow_up_black_18dp);
            labelContainer.setVisibility(View.VISIBLE);
            bottomSpace.setVisibility(View.VISIBLE);
        } else {
            expandCollapseButton.setImageResource(R.drawable.outline_keyboard_arrow_down_black_18dp);
            labelContainer.setVisibility(View.GONE);
            bottomSpace.setVisibility(View.GONE);
        }
        //clearing the existing image
        photoView.setImageDrawable(null);
        if (photo != null) {

            String url = photo.getImageSrc();

            try {
                if (url.startsWith("http://")) {
                    url = url.replace("http://", "https://");
                }
                Glide.with(photoView.getContext()).asBitmap().load(url).addListener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {

                        FirebaseVisionImageLabeler labeler = FirebaseVision.getInstance()
                                .getOnDeviceImageLabeler();
                        FirebaseVisionImage visionImage = FirebaseVisionImage.fromBitmap(resource);
                        labeler.processImage(visionImage)
                                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionImageLabel>>() {
                                    @Override
                                    public void onSuccess(List<FirebaseVisionImageLabel> labels) {

                                        labelContainer.setHasFixedSize(true);
                                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(labelContainer.getContext());
                                        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                                        labelContainer.setLayoutManager(linearLayoutManager);
                                        labelContainer.setAdapter(new LabelAdapter(labels));

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d("Log123", "Failed with: " + e.getLocalizedMessage());
                                    }
                                });
                        return false;
                    }
                }).into(photoView);
            } catch (Exception e) {
                e.printStackTrace();
            }

            photoName.setText(photo.getRover().getName());
            id.setText(String.valueOf(photo.getId()));
        }
    }
}
