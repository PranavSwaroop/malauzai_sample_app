package com.example.malauzai.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.malauzai.model.Photo;

import java.util.ArrayList;
import java.util.List;

public class PhotoViewModel extends ViewModel {


    private List<Photo> originalData = new ArrayList<>();
    private List<Photo> oldFilteredPosts = new ArrayList<>();
    private MutableLiveData<List<Photo>> filteredPosts = new MutableLiveData<>();



    public void setItems(List<Photo> photos) {
        originalData.addAll(photos);
        oldFilteredPosts.addAll(photos);
        filteredPosts.postValue(photos);
    }

    public void filterItems(String filterText) {
        List<Photo> filteredList = new ArrayList<>();
        for (Photo photo : originalData) {
            if (filterText.isEmpty() || String.valueOf(photo.getId()).startsWith(filterText)) {
                filteredList.add(photo);
            }
        }
        filteredPosts.postValue(filteredList);
    }

    public MutableLiveData<List<Photo>> getFilteredData() {
        return filteredPosts;
    }

    public List<Photo> getOldFilteredList() {
        return oldFilteredPosts;
    }

    public void changeOldData() {
        oldFilteredPosts.clear();
        oldFilteredPosts.addAll(filteredPosts.getValue());
    }
}
